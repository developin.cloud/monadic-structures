import './option/lift.spec';
import './option/floatMap.spec';
import './option/unwrap.spec';
import './option/chainable-option.spec';

import './either/common.spec';
import './either/flatMap.spec';
import './either/lift.spec';
import './either/map.spec';
import './either/unwrap.spec';
import './either/chainable-either.spec';

import './utils/utils.spec';