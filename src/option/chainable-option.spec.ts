import { Of } from "./chainable-option";

describe('Full integration test', () => {

    it('should perform all monadic operations', () => {
       
        
        const result = Of
            .lift(() => ({ a: 2, b: 'test' }))
            .flatMap(s => ({ b: s.a*10, c: `${s.b}--${s.b}` }))
            .flatMap(s => `[${s.b}]:${s.c.substring(0, 4)}`)
            .unwrap('Error');

        expect(result).toBe('[20]:test');

    });

})