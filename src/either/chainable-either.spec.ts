import { Of } from "./chainable-either";
import { right } from "./lift";

describe('Full integration test', () => {

    it('should perform all monadic operations', () => {
       
        const result = Of
            .lift(() => ({ a: 2, b: 'test' }))
            .flatMap(s => right({ b: s.a*10, c: `${s.b}--${s.b}` }))
            .map(s => `[${s.b}]:${s.c.substring(0, 4)}`)
            .unwrap(() => '');

        expect(result).toBe('[20]:test');

    });

    it('should handle errors', () => {
       
        const result = Of
            .lift(() => ({ a: 2, b: 'test' }))
            .map<string>(() => { throw new Error('X') })
            .flatMap(s => right(s.toUpperCase()))
            .unwrap(() => 'Error');

        expect(result).toBe('Error');

    });

})