import { Left, Either, isLeft, isRight, getValue, getError, Right } from "./common";
import { flatMap } from "./flatMap";
import { lift, right } from "./lift";

describe('FlatMap operator on Either', () => {

    describe('if state is Left', () => {

        const testErr: Error = new Error('Test');
        const left: Either<number> = { type: 'failure', error: testErr };

        it('should return Left', () => {
            const newEither = flatMap<number, number>((n => right(n + 10)), left);
            expect(isLeft(newEither)).toBe(true);
            expect(getError(<Left>newEither)).toBe(testErr);
        });

    });

    describe('if state is Right', () => {

        const rightM: Either<number> = { type: 'success', value: 10 };

        it('should return mapped Value', () => {
            const newEither = flatMap(s => right(s + 10), rightM);
            expect(isRight(newEither)).toBe(true);
            expect(getValue(<Right<number>>newEither)).toBe(20);
        });

    });

});