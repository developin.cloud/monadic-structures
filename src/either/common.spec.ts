import { isRight, Right, isLeft, getError, Either, Left, getValue } from "./common";

describe('when type is success', () => {

    const rightM: Either<Number> = { type: 'success', value: 1 };

    it('isRight() should return true', () => {
        const right = isRight(rightM);
        expect(right).toBe(true);
    });

    it('isLeft() should return false', () => {
        const left = isLeft(rightM);
        expect(left).toBe(false);
    });

    it('getError() should return undefined', () => {
        const error = getError(<any>rightM);
        expect(error).toBeUndefined();
    });

    it('getValue() should return value', () => {
        const value = getValue(rightM);
        expect(value).toBe(1);
    });

});

describe('when type is failure', () => {

    const testError: Error = new Error('Test');
    const leftM: Either<Number> = { type: 'failure', error: testError };

    it('isRight() should return false', () => {
        const right = isRight(leftM);
        expect(right).toBe(false);
    });

    it('isLeft() should return true', () => {
        const left = isLeft(leftM);
        expect(left).toBe(true);
    });

    it('getError() should return error', () => {
        const error = getError(leftM);
        expect(error).toBe(testError);
    });

    it('getValue() should return undefined', () => {
        const value = getValue(<any>leftM);
        expect(value).toBeUndefined();
    });

});

