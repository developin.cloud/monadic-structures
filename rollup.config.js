import typescript from 'rollup-plugin-typescript2';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';

import pkg from './package.json';

const tsOverrides = {
    exclude: [
        "node_modules",
        ...(process.env.TEST === 'true' ? [] : ["src/**/*.spec.ts"])
    ]
}

const input = process.env.TEST === 'true' ? 'src/test.ts' : 'src/index.ts';
console.log(input);
const external = [
    ...Object.keys(pkg.dependencies || []),
    ...Object.keys(pkg.peerDependencies || [])
];
const plugins = [
    typescript({
        typescript: require('typescript'),
        tsconfig: './tsconfig.json',
        tsconfigOverride: tsOverrides
    })
]
export default [{
    input,
    output: [
        { file: pkg.main, format: 'cjs' },
        { file: pkg.module, format: 'es'}
    ],
    external,
    plugins
}, {
    input,
    output: [
        { file: pkg.browser, format: 'umd', name: 'monadic-structures' }
    ],
    external,
    plugins: [
        ...plugins,
        resolve(),
        commonjs()
    ]
}]

