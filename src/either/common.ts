export interface Right<TRValue> {
    type: 'success',
    value: TRValue;
}

export interface Left {
    type: 'failure'
    error: Error
}

export type Either<RValue>  =
    | Right<RValue>
    | Left

export function isRight<RValue>(x: Either<RValue>) { return x.type === 'success'; };
export function isLeft<RValue>(x: Either<RValue>) { return x.type === 'failure'; };

export const getError = (x: Left) => x.error;
export const getValue = <RValue>(x: Right<RValue>) => x.value;