module.exports = function(config) {

    config.set({
        frameworks: ['jasmine'],
        files: ['dist/index.js'],
        plugins: ['karma-jasmine', 'karma-phantomjs-launcher'],
        browsers: ['PhantomJS'],
    })

}