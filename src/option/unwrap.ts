import { Option } from "./common";
import { hasValue } from "../utils";

export const unwrap = <T>(option: Option<T>, defaultValue: T):T => {
    if(hasValue(option)) {
        return <T>option;
    }
    return defaultValue;
}