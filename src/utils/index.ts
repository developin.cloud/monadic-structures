export type ValueFactory<T> = () => T;
export type Liftable<T> = T | ValueFactory<T>;

export const hasValue = <T>(s: T) => s != null;
export const isFunction = (s: any) => s.__proto__.constructor.name === "Function";