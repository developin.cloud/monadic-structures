import { isLeft, Either, Left, getValue, Right } from "./common";

export const map = <S, T>(func: (s: S) => T, current: Either<S>): Either<T> => {
    if(isLeft(current)) {
        return current as Left;
    }

    try {
        const value: T = func(getValue(current as Right<S>))
        return { type: 'success', value };
    } catch(error) {
        return { type: 'failure', error };
    }
}