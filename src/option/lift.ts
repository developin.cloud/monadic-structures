import { Liftable, hasValue, isFunction, ValueFactory } from "../utils";
import { Option } from "./common";

export const lift = <T>(valueOrFunction: Liftable<T>): Option<T> => {
    if(!hasValue(valueOrFunction)) {
        return undefined;
    }

    if(isFunction(valueOrFunction)) {
        const value = (<ValueFactory<T>>valueOrFunction)();
        return hasValue(value) ? value : undefined;
    }
    
    return <T>valueOrFunction;
}