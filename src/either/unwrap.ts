import { Either, isLeft, getError, Left, getValue, Right } from "./common";

export const unwrap = <T>(value: Either<T>, defaultAction: (e: Error) => T): T => {
    if(isLeft(value)) {
        return defaultAction(getError(value as Left));
    } else {
        return getValue(value as Right<T>);
    }   
}