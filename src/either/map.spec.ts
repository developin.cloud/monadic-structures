import { Left, Either, isLeft, isRight, getValue, getError, Right } from "./common";
import { map } from "./map";

describe('Map operator on Either', () => {

    describe('if state is Left', () => {

        const testErr: Error = new Error('Test');
        const left: Either<number> = { type: 'failure', error: testErr };

        it('should return Left', () => {
            const newEither = map<number, number>((n => n + 10), left);
            expect(isLeft(newEither)).toBe(true);
            expect(getError(<Left>newEither)).toBe(testErr);
        });

    });

    describe('if state is Right', () => {

        const right: Either<number> = { type: 'success', value: 10 };

        describe('and mapping function return proper value', () => {

            it('should return mapped Value', () => {
                const newEither = map(s => s + 10, right);
                expect(isRight(newEither)).toBe(true);
                expect(getValue(<Right<number>>newEither)).toBe(20);
            });

        });

        describe('and mapping function throws', () => {
            
            it('should return Left with error', () => {

                const err = new Error('Test');
                const newEither = map(s => { throw err; }, right);

                expect(isLeft(newEither)).toBe(true);
                expect(getError(<Left>newEither)).toBe(err);
            });
        })

    });

});