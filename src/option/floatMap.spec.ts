import { Option, flatMap } from ".";

describe('FlatMap operator on options', () => {
    describe('if state is undefined', () => {

        let option: Option<number>;

        beforeEach(() => {
            option = flatMap<number, number>(s => s * 10, undefined);
        });

        it('should return undefined', () => {
            expect(option).toBeUndefined();
        });

    });

    describe('if state is defined', () => {


        let option: Option<number>;

        beforeEach(() => {
            option = flatMap(s => s * 10, 10);
        });

        it('should run mapping function', () => {
            expect(option).toBe(100);
        });

    });


})