import { Liftable, hasValue, isFunction, ValueFactory } from "../utils";
import { Either, Left, Right } from "./common";

export const left = (error: Error): Left => ({ type: 'failure', error });
export const right = <T>(value: T): Right<T> => ({ type: 'success', value });


export const lift = <TR>(valueOrFunction: Liftable<TR>): Either<TR> => {
    if(!hasValue(valueOrFunction)) {
        return left(new Error('Value provided in lift is not provided'));
    }

    if(isFunction(valueOrFunction)) {
        try {
            const value = (<ValueFactory<TR>>valueOrFunction)();
            return right(value);
        } catch(error) {
            return left(error);
        }
    }

    return right(<TR>valueOrFunction);    
}