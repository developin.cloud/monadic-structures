import { Either, isLeft, Left, getValue, Right } from "./common";

export const flatMap = <S, T>(func: (s: S) => Either<T>, current: Either<S>): Either<T> => {
    if(isLeft(current)) {
        return current as Left;
    }

    return func(getValue(current as Right<S>))
}