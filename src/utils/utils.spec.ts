import { hasValue } from ".";

describe('hasValue', () => {
    
    describe('for string value', () => {

        let hasValueResult: boolean;

        beforeEach(() => {
            hasValueResult = hasValue('test');
        });

        it('should return true', () => {
            expect(hasValueResult).toBe(true);
        });
    });

    describe('for numeric value', () => {
        let hasValueResult: boolean;

        beforeEach(() => {
            hasValueResult = hasValue(1);
        });

        it('should return true', () => {
            expect(hasValueResult).toBe(true);
        });
    });

    describe('for object value', () => {
        let hasValueResult: boolean;

        beforeEach(() => {
            hasValueResult = hasValue({});
        });

        it('should return true', () => {
            expect(hasValueResult).toBe(true);
        });
    });

    describe('for null value', () => {
        let hasValueResult: boolean;

        beforeEach(() => {
            hasValueResult = hasValue(null);
        });

        it('should return false', () => {
            expect(hasValueResult).toBe(false);
        });
    });

    describe('for undefined value', () => {
        let hasValueResult: boolean;

        beforeEach(() => {
            hasValueResult = hasValue(undefined);
        });

        it('should return false', () => {
            expect(hasValueResult).toBe(false);
        });
    })
})