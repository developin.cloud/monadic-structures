import { unwrap } from "./unwrap";
import { Either } from "./common";
import { left, right } from "./lift";

describe('Unwrapping Either', () => {

    describe('when it is Left', () => {

        const err = new Error('test');
        const either = left(err);
        let value: number;
        const defaultAction = jasmine.createSpy('defaultAction').and.returnValue(42); 

        beforeEach(() => {
            
            value = unwrap<number>(either, defaultAction);
        });

        it('should return default value', () => {
            expect(value).toBe(42);
        });

        it('should call defaultAction', () => {
            expect(defaultAction).toHaveBeenCalledWith(err);
        });

    });

    describe('when it is Right', () => {
        
        let value: number;
        const monadValue = 20;
        const defaultAction = jasmine.createSpy('defaultAction').and.returnValue(42);
        const either = right(monadValue);

        beforeEach(() => {
            value = unwrap(either, defaultAction);
        });

        it('should return proper value', () => {
            expect(value).toBe(monadValue);
        });

        it('should not call defaultAction', () => {
            expect(defaultAction).toHaveBeenCalledTimes(0);
        })
    });

})