import { Either } from "./common";
import { Liftable } from "../utils";
import { lift } from "./lift";
import { flatMap } from "./flatMap";
import { map } from "./map";
import { unwrap } from ".";

export class Of<S> {

    private constructor(private state: Either<S>) { }

    static lift<X>(valueOrFunction: Liftable<X>): Of<X> {
        return new Of<X>(lift(valueOrFunction));
    }

    flatMap<T>(func: (s: S) => Either<T>): Of<T> {
        return new Of<T>(flatMap(func, this.state));
    }

    map<T>(func: (s: S) => T): Of<T> {
        return new Of<T>(map(func, this.state));
    }

    unwrap(defaultAction: (e: Error) => S) {
        return unwrap(this.state, defaultAction);
    }

}