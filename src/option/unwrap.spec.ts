import { unwrap } from "./unwrap";

describe('Unwrapping Option', () => {

    describe('when it does not have value', () => {

        let value: number;
        const defaultValue = 10;

        beforeEach(() => {
            value = unwrap<number>(undefined, defaultValue);
        });

        it('should return default value', () => {
            expect(value).toBe(defaultValue);
        });

    });

    describe('when it has value', () => {
        let value: number;
        const monadValue = 20;
        const defaultValue: number = 10;

        beforeEach(() => {
            value = unwrap(monadValue, defaultValue);
        });

        it('should return default value', () => {
            expect(value).toBe(monadValue);
        });
    });

})