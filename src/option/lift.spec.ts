import { Option } from './common';
import { lift } from './lift';

describe('Lifting option', () => {
    
    describe('by value', () => {

        describe('with defined numeric value', () => {

            let option: Option<number>;

            beforeEach(() => {
                option = lift(1);
            })

            it('should be able to get value', () => {
                expect(option).toBe(1);
            });
        });

        describe('with defined string value', () => {

            let option: Option<string>;
            const string = 'hello';

            beforeEach(() => {
                option = lift(string);
            })

            it('should be able to get value', () => {
                expect(option).toBe(string);
            });

        });

        describe('with defined object value', () => {

            let option: Option<{ name: string, age: number }>;
            const obj = { name: 'John', age: 42 };

            beforeEach(() => {
                option = lift(obj);
            })

            it('should be able to get value', () => {
                expect(option).toBe(obj);
            });

        });

        describe('with undefined value', () => {

            let option: Option<number>;

            beforeEach(() => {
                option = lift(undefined);
            });

            it('should be able to get value', () => {
                expect(option).toBe(undefined);
                expect(option).toBeUndefined();
            });
        });
    });

    describe('by factory function which returns', () => {

        describe('defined numeric value', () => {

            let option: Option<number>;

            beforeEach(() => {
                option = lift(() => 1);
            })

            it('should be able to get value', () => {
                expect(option).toBe(1);
            });
        });

        describe('defined string value', () => {

            let option: Option<string>;
            const string = 'hello';

            beforeEach(() => {
                option = lift(() => string);
            })

            it('should be able to get value', () => {
                expect(option).toBe(string);
            });

        });

        describe('defined object value', () => {

            let option: Option<{ name: string, age: number }>;
            const obj = { name: 'John', age: 42 };

            beforeEach(() => {
                option = lift(() => obj);
            })

            it('should be able to get value', () => {
                expect(option).toBe(obj);
            });

        });

        describe('undefined value', () => {

            let option: Option<number>;

            beforeEach(() => {
                option = lift(() => undefined);
            });

            it('should be able to get value', () => {
                expect(option).toBe(undefined);
                expect(option).toBeUndefined();
            });
        });

    });

});