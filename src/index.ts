import * as o from './option';
import * as e from './either';

export const option = o;
export const either = e;
