import { Option } from "./common";
import { Liftable } from "../utils";
import { lift } from "./lift";
import { flatMap } from "./flatMap";
import { unwrap } from "./unwrap";


export class Of<S> {

    private constructor(private value: Option<S>) {}

    static lift<X>(valueOrFunction: Liftable<X>): Of<X> {
        return new Of<X>(lift(valueOrFunction));
    }

    flatMap<T>(func: (s: S) => Option<T>): Of<T> {
        return new Of<T>(flatMap(func, this.value));
    }

    unwrap(defaultValue: S) {
        return unwrap(this.value, defaultValue);
    }
}