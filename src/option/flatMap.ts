import { Option } from "./common";
import { hasValue } from "../utils";

export const flatMap = <S, T>(func: (s: S) => Option<T>, current: Option<S>): Option<T> => {
    if(hasValue(current)) {
        return func(current!);
    }

    return undefined;
}