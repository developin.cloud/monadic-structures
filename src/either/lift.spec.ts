import { Either, isRight, getValue, Right, isLeft, getError, Left } from "./common";
import { lift } from "./lift";
import { either } from "..";

describe('Lifting Either', () => {
    
    describe('by value', () => {

        describe('with defined numeric value', () => {

            let either: Either<number>;

            beforeEach(() => {
                either = lift(1);
            })

            it('should be able to get value', () => {
                expect(isRight(either)).toBe(true);
                expect(getValue(<Right<number>>either)).toBe(1);
            });
        });

        describe('with defined string value', () => {

            let either: Either<string>;
            const string = 'hello';

            beforeEach(() => {
                either = lift(string);
            })

            it('should be able to get value', () => {
                expect(isRight(either)).toBe(true);
                expect(getValue(<Right<string>>either)).toBe(string);
            });

        });

        describe('with defined object value', () => {

            let either: Either<{ name: string, age: number }>;
            const obj = { name: 'John', age: 42 };

            beforeEach(() => {
                either = lift(obj);
            })

            it('should be able to get value', () => {
                expect(isRight(either)).toBe(true);
                expect(getValue(<any>either)).toBe(obj);
            });

        });
    });

    describe('by factory function which', () => {

        describe('throws Error', () => {

            const testErr = new Error('Test');
            let either: Either<number>;

            beforeEach(() => {
                either = lift(() => { throw testErr });
            });

            it('should be able to get error', () => {
                expect(isLeft(either)).toBe(true);
                expect(getError(<Left>either)).toBe(testErr);
            });
        });

        describe('returns proper value', () => {
            
            const testString = 'hello';
            let either: Either<string>;

            beforeEach(() => {
                either = lift(() => testString);
            });

            it('should be able to get value', () => {
                expect(isRight(either)).toBe(true);
                expect(getValue(<Right<string>>either)).toBe(testString);
            });
        });
    });

});